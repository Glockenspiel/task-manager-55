package ru.t1.sukhorukova.tm.command.system;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.api.endpoint.ISystemEndpoint;
import ru.t1.sukhorukova.tm.api.service.ICommandService;
import ru.t1.sukhorukova.tm.api.service.IPropertyService;
import ru.t1.sukhorukova.tm.command.AbstractCommand;
import ru.t1.sukhorukova.tm.enumerated.Role;

@Component
public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return getLocatorService().getCommandService();
    }

    protected ISystemEndpoint getSystemEndpoint() {
        return getLocatorService().getSystemEndpoint();
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
