package ru.t1.sukhorukova.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.sukhorukova.tm.api.service.ITokenService;

@Getter
@Setter
@Service
public final class TokenService implements ITokenService {

    @Nullable
    private String token;

}
