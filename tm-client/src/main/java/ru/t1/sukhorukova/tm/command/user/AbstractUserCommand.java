package ru.t1.sukhorukova.tm.command.user;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.sukhorukova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.sukhorukova.tm.api.endpoint.IUserEndpoint;
import ru.t1.sukhorukova.tm.command.AbstractCommand;

@Component
public abstract class AbstractUserCommand extends AbstractCommand {

    protected IUserEndpoint getUserEndpoint() {
        return getLocatorService().getUserEndpoint();
    }

    protected IAuthEndpoint getAuthEndpoint() {
        return getLocatorService().getAuthEndpoint();
    }

    @Nullable @Override
    public String getArgument() {
        return null;
    }

}
