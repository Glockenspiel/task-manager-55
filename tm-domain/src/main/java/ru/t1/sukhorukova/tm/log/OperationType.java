package ru.t1.sukhorukova.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE

}
