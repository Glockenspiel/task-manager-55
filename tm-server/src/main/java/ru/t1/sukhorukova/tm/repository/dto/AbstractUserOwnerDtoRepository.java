package ru.t1.sukhorukova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.api.repository.dto.IUserOwnerDtoRepository;
import ru.t1.sukhorukova.tm.dto.model.AbstractUserOwnerModelDTO;

import javax.persistence.EntityManager;

public abstract class AbstractUserOwnerDtoRepository<M extends AbstractUserOwnerModelDTO> extends AbstractDtoRepository<M> implements IUserOwnerDtoRepository<M> {

    protected AbstractUserOwnerDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void add(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        entityManager.merge(model);
    }

}
